package test;

import controller.BunteRechteckeController;
import model.Rechteck;

public class RechteckTest {
	
	public static void rechteckeTesten() {
		
		boolean entscheider = true;
		
		Rechteck wahreRechteck = new Rechteck(0,0,1200,1000);
		
		Rechteck[] zufaelligeRechtecke = new Rechteck [50000];
		
		for(Rechteck r : zufaelligeRechtecke) {
			r = Rechteck.generiereZufallsRechteck();
			if(wahreRechteck.enthaelt(r)) {
				System.out.println("Recheck "+r+": bestanden!");
			} else {
			System.out.println("Recheck "+r+": nicht bestanden!");
			entscheider = false;
			}
		}
		System.out.println("Gesamtergebnis:"+entscheider);
		
	}

	public static void main(String[] args) {
		rechteckeTesten();

		BunteRechteckeController brc = new BunteRechteckeController();
		
		//5 Rechtecke mit parameterlosen Konstruktor
		Rechteck r0 = new Rechteck();
		r0.setX(10);
		r0.setY(10);
		r0.setBreite(30);
		r0.setHoehe(40);
		
		Rechteck r1 = new Rechteck();
		r1.setX(25);
		r1.setY(25);
		r1.setBreite(100);
		r1.setHoehe(20);
		
		Rechteck r2 = new Rechteck();
		r2.setX(260);
		r2.setY(10);
		r2.setBreite(200);
		r2.setHoehe(100);
		
		Rechteck r3 = new Rechteck();
		r3.setX(5);
		r3.setY(500);
		r3.setBreite(300);
		r3.setHoehe(25);
		
		Rechteck r4 = new Rechteck();
		r4.setX(100);
		r4.setY(100);
		r4.setBreite(100);
		r4.setHoehe(100);
		//5 Rechtecke mit vollparametrisierten Konstruktor
		Rechteck r5 = new Rechteck(200,200,200,200);
		Rechteck r6 = new Rechteck(800,400,20,20);
		Rechteck r7 = new Rechteck(800,450,20,20);
		Rechteck r8 = new Rechteck(850,400,20,20);
		Rechteck r9 = new Rechteck(855,455,25,25);
		// to-String Methoden test
		System.out.println(r0.toString().equals("Rechteck [x=10, y=10, breite=30, hoehe=40]"));
		
		//Pr�fen ob Rechteckerstellung mit negativen Breiten und H�hen m�glich ist.
	    Rechteck eck10 = new Rechteck(-4,-5,-50,-200);
	    System.out.println(eck10); //Rechteck [x=-4, y=-5, breite=50, hoehe=200]
	    Rechteck eck11 = new Rechteck();
	    eck11.setX(-10);
	    eck11.setY(-10);
	    eck11.setBreite(-200);
	    eck11.setHoehe(-100);
	    System.out.println(eck11);//Rechteck [x=-10, y=-10, breite=200, hoehe=100]
	    
	    //Test f�r Methode enthaelt(Rechteck r)
	    System.out.println(new Rechteck(860, 150, 50, 50).enthaelt(new Rechteck(870, 160, 40, 40))+"");
	    System.out.println(new Rechteck(870, 160, 40, 40).enthaelt(new Rechteck(860, 150, 50, 50))+"");
	    
		brc.add(r0);
		brc.add(r1);
		brc.add(r2);
		brc.add(r3);
		brc.add(r4);
		brc.add(r5);
		brc.add(r6);
		brc.add(r7);
		brc.add(r8);
		brc.add(r9);
		brc.add(eck10);
		brc.add(eck11);

		
		System.out.println(brc.toString());
	}

}
