package model;

import java.util.Random;

public class Rechteck {

	private Punkt p;
	private int breite;
	private int hoehe;
	private static Random random = new Random();
	
	public Rechteck() {
		this.p = new Punkt(0,0);
		this.setBreite(0);
		this.setHoehe(0);
	}
	
	public Rechteck(int x, int y, int breite, int hoehe) {
		this.p = new Punkt(x,y);
		this.setBreite(breite);
		this.setHoehe(hoehe);
	}
	
	public int getX() {
		return this.p.getX();
	}
	
	public void setX(int x) {
		this.p.setX(x);
	}
	
	public int getY() {
		return this.p.getY();
	}
	
	public void setY(int y) {
		this.p.setY(y);
	}
	
	public int getBreite() {
		return breite;
	}
	
	public void setBreite(int breite) {
		this.breite = Math.abs(breite);
	}
	
	public int getHoehe() {
		return hoehe;
	}
	
	public void setHoehe(int hoehe) {
		this.hoehe = Math.abs(hoehe);
	}
	
	public boolean enthaelt(int x, int y) {
		
		if(this.p.getX() <= x && this.p.getY() <= y && x <= this.p.getX() + this.breite && y <= this.p.getY() + this.hoehe)
/**  ------------- 
	|             |
	|			  |		
 y  | 			  |
	|			  |
	|			  |
	 -------------
	 		x
*/
			return true;
		
		return false;
	}
	
	public boolean enthaelt(Punkt p) {
		return enthaelt(p.getX(), p.getY());
	}

	public boolean enthaelt(Rechteck r) {
		return enthaelt(r.p) && enthaelt(r.getX()+r.getBreite(), r.getY()+r.getHoehe());
	}
	
	public static Rechteck generiereZufallsRechteck() {
		int x = random.nextInt(1200)+1;
		int y = random.nextInt(1000)+1;
		int breite =random.nextInt(1200)+1 - x;
		int hoehe = random.nextInt(1200)+1 - y;
		
		return new Rechteck(x,y,breite,hoehe);
	}
	
	@Override
	public String toString() {
		return "Rechteck [x=" + this.p.getX() + ", y=" + this.p.getY() + ", breite=" + breite + ", hoehe=" + hoehe + "]";
	}
}
