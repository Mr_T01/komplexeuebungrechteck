package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Eingabemaske extends JFrame {

	private JPanel pnl_GrundPanel;
	private JPanel pnl_EingabePanel;
	private JTextField tfd_x;
	private JTextField tfd_y;
	private JTextField tfd_breite;
	private JTextField tfd_hoehe;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Eingabemaske frame = new Eingabemaske();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Eingabemaske() {
		BunteRechteckeController brc = new BunteRechteckeController();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 300);
		pnl_GrundPanel = new JPanel();
		pnl_GrundPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pnl_GrundPanel);
		pnl_GrundPanel.setLayout(new BorderLayout(0, 0));
		
		pnl_EingabePanel = new JPanel();
		pnl_GrundPanel.add(pnl_EingabePanel, BorderLayout.CENTER);
		pnl_EingabePanel.setLayout(null);
		
		tfd_x = new JTextField();
		tfd_x.setBounds(115, 11, 86, 20);
		pnl_EingabePanel.add(tfd_x);
		tfd_x.setColumns(10);
		
		tfd_y = new JTextField();
		tfd_y.setColumns(10);
		tfd_y.setBounds(211, 11, 86, 20);
		pnl_EingabePanel.add(tfd_y);
		
		tfd_breite = new JTextField();
		tfd_breite.setColumns(10);
		tfd_breite.setBounds(307, 11, 86, 20);
		pnl_EingabePanel.add(tfd_breite);
		
		tfd_hoehe = new JTextField();
		tfd_hoehe.setColumns(10);
		tfd_hoehe.setBounds(403, 11, 86, 20);
		pnl_EingabePanel.add(tfd_hoehe);
		
		JLabel lbl_x = new JLabel("x");
		lbl_x.setBounds(149, 42, 46, 14);
		pnl_EingabePanel.add(lbl_x);
		
		JLabel lbl_y = new JLabel("y");
		lbl_y.setBounds(251, 42, 46, 14);
		pnl_EingabePanel.add(lbl_y);
		
		JLabel lbl_breite = new JLabel("breite");
		lbl_breite.setBounds(338, 42, 46, 14);
		pnl_EingabePanel.add(lbl_breite);
		
		JLabel lbl_hoehe = new JLabel("hoehe");
		lbl_hoehe.setBounds(431, 42, 46, 14);
		pnl_EingabePanel.add(lbl_hoehe);
		
		JButton btn_speichern = new JButton("speichern");
		btn_speichern.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				int x = Integer.parseInt(tfd_x.getText());
				int y = Integer.parseInt(tfd_y.getText());
				int breite = Integer.parseInt(tfd_breite.getText());
				int hoehe = Integer.parseInt(tfd_hoehe.getText());
				
				brc.add(new Rechteck(x,y,breite,hoehe));
			}
		});
		btn_speichern.setBounds(251, 128, 103, 23);
		pnl_EingabePanel.add(btn_speichern);
	}
}
