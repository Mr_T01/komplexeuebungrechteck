package view;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import controller.BunteRechteckeController;
import model.Rechteck;

@SuppressWarnings("serial")
public class Zeichenflaeche extends JPanel{

	private BunteRechteckeController brc;
	
	public Zeichenflaeche() {
		super();
	}
	
	public Zeichenflaeche(BunteRechteckeController brc) {
		super();
		this.brc = brc;
	}
	
	@Override
	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		
		for(Rechteck rechteck : this.brc.getRechtecke()) {
			g.drawRect(rechteck.getX(), rechteck.getY(), rechteck.getBreite(), rechteck.getHoehe());
		}
	}
	
}
