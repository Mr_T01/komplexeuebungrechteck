package controller;

import java.util.LinkedList;

import model.Rechteck;

public class BunteRechteckeController {

	private LinkedList<Rechteck> rechtecke;
	
	public BunteRechteckeController() {
		this.rechtecke = new LinkedList<Rechteck>();
	}
	
	public void add(Rechteck rechteck) {
		this.rechtecke.add(rechteck);
	}
	
	public void reset() {
		this.rechtecke.clear();
	}
	
	public LinkedList<Rechteck> getRechtecke() {
		return rechtecke;
	}
	
	public void generiereZufallsRechtecke(int anzahl) {
		this.reset();
		Rechteck[] rechtecke = new Rechteck [anzahl];
		for(Rechteck r : rechtecke) {
			r = Rechteck.generiereZufallsRechteck();
			this.add(r);
		}
	}

	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}

	public static void main(String[] args) {
		

	}

}
